package ru.arubtsova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.enumerated.Role;
import ru.arubtsova.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "remove user by login.";
    }

    @Override
    public void execute() {
        System.out.println("Remove User:");
        System.out.println("Enter Login:");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
        System.out.println("User " + login + " was successfully removed");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
