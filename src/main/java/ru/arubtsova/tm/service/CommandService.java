package ru.arubtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.arubtsova.tm.api.repository.ICommandRepository;
import ru.arubtsova.tm.api.service.ICommandService;
import ru.arubtsova.tm.command.AbstractCommand;

import java.util.List;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }

}
